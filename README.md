# rocket 

console based graphics

clearly influenced by https://github.com/mtoyoda/sl

To build:
    `make rocket`
    
To install:
    `make rocket`

To uninstall:
    `sudo make uninstall`

To clean:
    `make clean`

TO DO :
1. add effects for propulsion and smoke
2. add variants of rockets
3. cleanup code
