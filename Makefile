CC=gcc
CFLAGS=-o
LIB=-lcurses

all: rocket

rocket: rocket.h rocket.c
	$(CC) $(CFLAGS) rocket rocket.c $(LIB)

install:
	cp rocket /usr/bin/

clean:
	rm -f rocket

uninstall: 
	rm -i /usr/bin/rocket
