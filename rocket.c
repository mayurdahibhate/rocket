#include <curses.h>
#include <signal.h>
#include <unistd.h>
#include "rocket.h"

int main(void)
{
    initscr();
    signal(SIGINT, SIG_IGN);
    noecho();
    curs_set(0);
    nodelay(stdscr, TRUE);
    leaveok(stdscr, TRUE);
    scrollok(stdscr, FALSE);

    static char *sp[21] = {
        rocketL1, rocketL2, rocketL3, rocketL4, rocketL5,
        rocketL6, rocketL7, rocketL8, rocketL9, rocketL10,
        rocketL11, rocketL12, rocketL13, rocketL14, rocketL15,
        rocketL16, rocketL17, rocketL18, rocketL19, rocketL20,
        rocketL21};

    for (int startline = LINES; startline >= 0 - rocketheight; startline--)
    {
        int lineno = startline > -rocketheight ? 0 : rocketheight + startline;
        int smoke = 5;

        for (int i = startline; i < LINES; i++)
        {
            if (lineno > rocketheight - 1)
            {
                mvprintw(i, COLS / 2 - 20, blank);
                if (smoke > 0) 
                {
                    mvprintw(i, COLS / 2 - 20, rocketsmoke);
                    smoke--;
                }
            }
            else if (lineno > -1)
                mvprintw(i, COLS / 2 - 20, sp[lineno]);

            lineno++;
        }

        refresh();
        usleep(100000);
    }
    mvcur(0, COLS - 1, LINES - 1, 0);
    endwin();

    return 0;
}
